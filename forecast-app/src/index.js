import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import CityPage from './CityPage';

import {
    BrowserRouter as Router,
    Route,
    Switch,
    Link
} from 'react-router-dom';

ReactDOM.render(
    <Router>
        <div>
            <Switch>
                <Route exact path="/" component={App}/>
                <Route exact path="/:forecastTerm/:cityName" component={CityPage}/>
                <Route render={() => {
                    return(
                         <div>
                             <h3>Application hasn't such page</h3>
                             <Link to="/">Visit the homepage</Link>
                         </div>
                    );
                }}/>
            </Switch>
        </div>
    </Router>, document.getElementById('root'));
registerServiceWorker();
