import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import './App.css';
import {withRouter} from 'react-router';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSearch() {
        if (this.state.value) {
            this.props.history.push(`1/${this.state.value}`);
        }
    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="weatherForecast">
                    <div className="searchFieldHolder" style={{textAlign:"center"}}>
                        <h1>Check weather in your city</h1>
                        <TextField
                            id="text-field-controlled"
                            hintText="Your city name"
                            onChange={this.handleChange}
                        />
                        <div >
                            <RaisedButton onClick={this.handleSearch} label="Find forecast"/>
                        </div>

                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default withRouter(App);
