import React, {Component} from 'react';
import {withRouter} from 'react-router';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import ForecastTable from './ForecastTable';

import {Link} from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import './ForecastInfo.css';


const TitleForecast = (props) => {
    let termTitle = props.term === 1 ? `Today` : `Next ${props.term} days`;
    let title = `${termTitle} weather forecast in ${props.cityName} (${props.country})`;
    return (
        <h1 className="titleForecast">{title}</h1>
    );
};

class ForecastInfo extends Component {
    state = {
        forecast: this.props.cityInfo,
        forecastTerm: parseInt(this.props.forecastTerm, 0),
        cityName: this.props.cityName
    };


    handleChange = (event, index, value) => this.props.history.push(`/${value}/${this.state.cityName}`);


    render() {
        return (
            <div className="forecastHolder">
                <div><Link to="/">Get forecast for another city</Link></div>
                <TitleForecast
                    cityName={this.state.forecast.location.name}
                    term={this.state.forecastTerm}
                    country={this.state.forecast.location.country}
                />
                <div className="selectHolder">
                    <MuiThemeProvider>
                        <SelectField
                            floatingLabelText="Forecast Term"
                            value={this.state.forecastTerm}
                            onChange={this.handleChange}
                            style={{width:"100%"}}
                        >
                            <MenuItem value={1} primaryText="Hourly"/>
                            <MenuItem value={5} primaryText="Ger forecast for next 5 days"/>
                            <MenuItem value={10} primaryText="Ger forecast for next 10 days"/>
                        </SelectField>
                    </MuiThemeProvider>
                </div>


                <div className="tableHolder">
                    {
                        this.state.forecast.forecast.forecastday.map((day) => {
                            return (
                                <ForecastTable
                                    key={day.date_epoch}
                                    forecast={day}
                                />
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

export default withRouter(ForecastInfo);