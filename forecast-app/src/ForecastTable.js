import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';


const tableStyle = {
    textAlign: 'center'
};

const ForecastDay = function (props) {
    const {iconSrc, iconText, temp_c, time} = props;
    return (
        <TableRow>
            <TableRowColumn style={tableStyle}>{time.split(' ').pop()}</TableRowColumn>
            <TableRowColumn style={tableStyle}>{temp_c} °C</TableRowColumn>
            <TableRowColumn style={tableStyle}>
                <img src={iconSrc} alt={iconText}/>
                <div>{iconText}</div>
            </TableRowColumn>
        </TableRow>
    );
};


const ForecastTable = function (props) {
    const {forecast} = props;
    return (
        <div>
            <h2 style={tableStyle}>Forecast for {forecast.date}</h2>
            <MuiThemeProvider>
                <Table>
                    <TableHeader
                        className="tableHeader"
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                            <TableHeaderColumn style={tableStyle}>Time</TableHeaderColumn>
                            <TableHeaderColumn style={tableStyle}>Temperature</TableHeaderColumn>
                            <TableHeaderColumn style={tableStyle}>Condition</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {
                            forecast.hour.map(hour =>
                                <ForecastDay
                                    key={hour.time_epoch}
                                    iconSrc={hour.condition.icon}
                                    iconText={hour.condition.text}
                                    temp_c={hour.temp_c}
                                    time={hour.time}
                                />
                            )
                        }
                    </TableBody>
                </Table>
            </MuiThemeProvider>
        </div>
    );
};

export default ForecastTable;