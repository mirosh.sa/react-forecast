import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-refetch';
import LinearProgress from 'material-ui/LinearProgress';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import config from './config';
import ForecastInfo from './ForecastInfo'

const CityPage = function (props) {
    const dataFetch = props.cityFetch;
    const cityName = props.match.params.cityName;
    const forecastTerm = props.match.params.forecastTerm;
    if (dataFetch.pending) {
        return (
            <MuiThemeProvider>
                <div>
                    <LinearProgress />
                </div>
            </MuiThemeProvider>
        );
    } else if (dataFetch.rejected) {
        return (
            <div>
                <div>I can't find any forecasts for "{cityName}" city</div>
                <Link to="/">Do another search</Link>
            </div>
        );
    } else if (dataFetch.fulfilled) {
        return <ForecastInfo
            cityInfo={dataFetch.value}
            cityName={cityName}
            forecastTerm={forecastTerm > 10 ? 10: forecastTerm}
        />
    }
};

export default connect((props) => ({
    cityFetch: `http://api.apixu.com/v1/forecast.json?key=${config._APIKEY_}&q=${props.match.params.cityName}&days=${props.match.params.forecastTerm}`
}))(CityPage)